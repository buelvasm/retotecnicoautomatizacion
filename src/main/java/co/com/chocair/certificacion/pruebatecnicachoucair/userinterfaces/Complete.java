package co.com.chocair.certificacion.pruebatecnicachoucair.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class Complete {
    public static final Target CREATEPASSWORD = Target.the("create password").
            locatedBy("//*[@id=\"password\"]");
    public static final Target CONFIRMPASSWORD = Target.the("confirm pasword").
            locatedBy("//*[@id=\"confirmPassword\"]");
    public static final Target CHECKTWO = Target.the("check two").
            locatedBy("//span[@class=\"checkmark signup-consent__checkbox error\"]");
    public static final Target CHECKTHREE = Target.the("check three").
            locatedBy("//span[@ng-class=\"{error: userForm.privacySetting.$error.required}\"]");
    public static final Target BUTTONCOMPLETE = Target.the("button complete").
            locatedBy("//*[@id=\"laddaBtn\"]");


}
