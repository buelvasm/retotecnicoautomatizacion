package co.com.chocair.certificacion.pruebatecnicachoucair.tasks;

import co.com.chocair.certificacion.pruebatecnicachoucair.userinterfaces.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegisterUserReto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Home.REGISTER),
                SendKeys.of("Manuel").into(Personal.FIRSTNAME),
                SendKeys.of("Buelvas").into(Personal.LASTNAME),
                SendKeys.of("buelvasmb@gmail.com").into(Personal.EMAIL),
                SendKeys.of("March").into(Personal.MONTH),
                SendKeys.of("6").into(Personal.DAY),
                SendKeys.of("1996").into(Personal.YEAR),
                Click.on(Personal.NEXTLOCATION),
                WaitUntil.the(Location.CITY,isEnabled()).forNoMoreThan(15).seconds(),
                Click.on(Location.BUTTONLOCATION),
                Click.on(Devices.SYSTEMOP),
                Click.on(Devices.WINDOWS),
                Click.on(Devices.VERSION),
                Click.on(Devices.WIN10),
                Click.on(Devices.LANGUAGE),
                Click.on(Devices.SPANISH),
                Click.on(Devices.MOBILE),
                Click.on(Devices.MOTOROLA),
                Click.on(Devices.MODEL),
                Click.on(Devices.MOTOG),
                Click.on(Devices.OPERATINGSYSTEMS),
                Click.on(Devices.ANDROID10),
                Click.on(Devices.BOTTONLASTFINAL),
                SendKeys.of("Choucair2021*").into(Complete.CREATEPASSWORD),
                SendKeys.of("Choucair2021*").into(Complete.CONFIRMPASSWORD),
                Click.on(Complete.CHECKTWO),
                Click.on(Complete.CHECKTHREE),
                Click.on(Complete.BUTTONCOMPLETE)
        );
    }
    public static RegisterUserReto makeinformation(){
        return instrumented(RegisterUserReto.class);
    }
}
