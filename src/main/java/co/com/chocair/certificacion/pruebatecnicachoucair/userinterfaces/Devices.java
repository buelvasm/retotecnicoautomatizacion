package co.com.chocair.certificacion.pruebatecnicachoucair.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class Devices {
    public static final Target SYSTEMOP= Target.the("systemop").
            locatedBy("(//span[@class=\"btn btn-default form-control ui-select-toggle\"])[1]");
    public static final Target WINDOWS= Target.the("windows").
            locatedBy("(//span[@class=\"ui-select-choices-row-inner\"])[2]");
    public static final Target VERSION= Target.the("version").
            locatedBy("(//span[@class=\"btn btn-default form-control ui-select-toggle\"])[2]");
    public static final Target WIN10= Target.the("win10").
            locatedBy("(//span[@class=\"ui-select-choices-row-inner\"])[16]");
    public static final Target LANGUAGE= Target.the("language").
            locatedBy("(//span[@class=\"btn btn-default form-control ui-select-toggle\"])[3]");
    public static final Target SPANISH= Target.the("spanish").
            locatedBy("(//span[@class=\"ui-select-choices-row-inner\"])[38]");
    public static final Target MOBILE= Target.the("mobile").
            locatedBy("(//span[@class=\"btn btn-default form-control ui-select-toggle\"])[4]");
    public static final Target MOTOROLA= Target.the("motorola").
            locatedBy("(//span[@class=\"ui-select-choices-row-inner\"])[24]");
    public static final Target MODEL= Target.the("model").
            locatedBy("(//span[@class=\"btn btn-default form-control ui-select-toggle\"])[5]");
    public static final Target MOTOG= Target.the("motog").
            locatedBy("(//span[@class=\"ui-select-choices-row-inner\"])[173]");
    public static final Target OPERATINGSYSTEMS= Target.the("operatingsystems").
            locatedBy("(//span[@class=\"btn btn-default form-control ui-select-toggle\"])[6]");
    public static final Target ANDROID10= Target.the("android10").
            locatedBy("(//span[@class=\"ui-select-choices-row-inner\"])[1]");
    public static final Target BOTTONLASTFINAL= Target.the("button last final").
            locatedBy("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/div[2]/div/a");


}
