package co.com.chocair.certificacion.pruebatecnicachoucair.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class Personal {

    public static final Target FIRSTNAME = Target.the("first name").
            locatedBy("//*[@id=\"firstName\"]");
    public static final Target LASTNAME = Target.the("last name").
            locatedBy("//*[@id=\"lastName\"]");
    public static final Target EMAIL = Target.the("email").
            locatedBy("//*[@id=\"email\"]");
    public static final Target MONTH = Target.the("month").
            locatedBy("//select[@id=\"birthMonth\"]");
    public static final Target DAY = Target.the("day").
            locatedBy("//select[@id=\"birthDay\"]");
    public static final Target YEAR = Target.the("year").
            locatedBy("//select[@id=\"birthYear\"]");
    public static final Target NEXTLOCATION = Target.the("button next").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/a"));

}
