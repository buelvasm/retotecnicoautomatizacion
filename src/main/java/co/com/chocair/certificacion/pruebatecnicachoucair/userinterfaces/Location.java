package co.com.chocair.certificacion.pruebatecnicachoucair.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Location {
    public static final Target CITY = Target.the("city").
            located(By.xpath("//*[@id=\"city\"]"));
    public static final Target BUTTONLOCATION= Target.the("button location").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a"));

}
