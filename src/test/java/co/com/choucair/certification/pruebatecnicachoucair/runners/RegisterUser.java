package co.com.choucair.certification.pruebatecnicachoucair.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/register_user.feature",
      glue = "co.com.choucair.certification.pruebatecnicachoucair.stepdefinitions",
      snippets = SnippetType.CAMELCASE)

public class RegisterUser {
}
